﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class MainPanel : IDataComponent
    {
        public Image Background { get; private set; }
        public Text TimerText { get; private set; }
        public Text ScoreText { get; private set; }

        public MainPanel()
        {
            Background = new Image("Background");
            TimerText = new Text("Font", "Time: 60", new Vector2(20, 10), Color.Red);
            ScoreText = new Text("Font", "Score: 0", new Vector2(20, 50), Color.Yellow);
        }

        public void RestoreData()
        {
            TimerText.SetActive(true);
            ScoreText.SetActive(true);
        }

        public List<IComponent> GetComponents()
        {
            var components = new List<IComponent>()
            {
                Background,
                TimerText,
                ScoreText,
            };
            return components; 
        }
    }
}
