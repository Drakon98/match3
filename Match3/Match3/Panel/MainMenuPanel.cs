﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class MainMenuPanel : IDataComponent
    {
        public Image Background { get; private set; }
        public Button PlayButton { get; private set; }
        public MusicPlayer MusicPlayer { get; private set; }

        public MainMenuPanel()
        {
            Background = new Image("Background");
            PlayButton = new Button(new Image("Button"), new Text("Font", "Play"), new Vector2(1920/2 - 100, 1020/2), Color.Green);
            MusicPlayer = new MusicPlayer(new List<string>() { "8.-Ori-and-the-Blind-Forest-OST-09-The-Spirit-Tree-_feat.-Aeralie-Brighton_" });
        }

        public List<IComponent> GetComponents()
        {
            var components = new List<IComponent>()
            {
                Background,
                PlayButton,
                MusicPlayer
            };
            return components;
        }

        public void RestoreData() { }
    }
}
