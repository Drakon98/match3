﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class GridPanel : IDataComponent
    {
        public List<IElement> ExistElements { get; private set; } = new List<IElement>()
        {
            new Element(Color.Green, 1),
            new Element(Color.Yellow, 1),
            new Element(Color.Red, 1),
            new Element(Color.Violet, 1),
            new Element(Color.Blue, 1)
        };
        public GridGameObject GridGameObject { get; private set; }

        public Dictionary<string, Image> Resources { get; private set; } = new Dictionary<string, Image>()
        {
            { "Cell", new Image("Cell", Vector2.Zero, new Color(new Vector4(255, 255, 0, 0))) },
            { "Element", new Image("Element") },
            { "Line", new Image("Line") },
            { "Bomb", new Image("Bomb") },
            { "DestroyerUp", new Image("DestroyerUp", Vector2.Zero, Color.Indigo) },
            { "DestroyerDown", new Image("DestroyerDown", Vector2.Zero, Color.Indigo) },
            { "DestroyerLeft", new Image("DestroyerLeft", Vector2.Zero, Color.Indigo) },
            { "DestroyerRight", new Image("DestroyerRight", Vector2.Zero, Color.Indigo) },
            { "Explosian", new Image("Boom", Vector2.Zero, Color.Yellow) }
        };

        public ResourceLoader<Image> GridResourceLoader { get; private set; }

        public GridPanel()
        {
            GridResourceLoader = new ResourceLoader<Image>(Resources);

            GridGameObject = new GridGameObject(ExistElements, GridResourceLoader, new Vector2(550, 100));
        }

        public List<IComponent> GetComponents() => new List<IComponent>() { GridGameObject };

        public void RestoreData() => GridGameObject.SetActive(true);
    }
}
