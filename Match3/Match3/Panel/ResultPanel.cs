﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class ResultPanel : IDataComponent
    {
        public Text GameOverText { get; private set; }
        public Button AcceptButton { get; private set; }

        public ResultPanel()
        {
            GameOverText = new Text("Font", "Game over", new Vector2(275, 250), Color.Red);
            AcceptButton = new Button(new Image("Button"), new Text("Font", "Ok"), new Vector2(250, 300), Color.Green);
        }

        public List<IComponent> GetComponents()
        {
            var components = new List<IComponent>()
            {
                GameOverText,
                AcceptButton
            };
            return components;
        }

        public void RestoreData()
        {
            GameOverText.SetActive(false);
            AcceptButton.SetActive(false);
        }
    }
}
