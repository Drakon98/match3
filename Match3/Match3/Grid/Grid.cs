﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class Grid : IDataRestore
    {
        public IElementBuilder ElementBuilder { get; private set; }
        public Point Size { get; private set; }

        public List<Cell> Cells { get; private set; } = new List<Cell>();

        public Grid(IElementBuilder elementBuilder, Point size)
        {
            ElementBuilder = elementBuilder;
            Size = size;
            for (int i = 0; i < Size.X * Size.Y; i++)
            {
                Cells.Add(new Cell());
                Cells[i].SetElement(ElementBuilder.Create());
            }
        }

        public List<Vector2> Build(int widthCell, int heightCell, Vector2 startPosition, int distance = 0)
        {
            var positions = new List<Vector2>();
            for (int i = 0; i < Size.X; i++)
            {
                for (int j = 0; j < Size.Y; j++)
                {
                    var x = i * Size.X + j;
                    positions.Add(new Vector2(startPosition.X + widthCell * j, startPosition.Y + heightCell * i));
                    var leftCell = j == 0 ? null : Cells[x - 1];
                    var rightCell = j == Size.X - 1 ? null : Cells[x + 1];
                    var upCell = i == 0 ? null : Cells[(i - 1) * Size.X + j];
                    var downCell = i == Size.Y - 1 ? null : Cells[(i + 1) * Size.X + j];
                    Cells[x].CreateNearbody(leftCell, rightCell, upCell, downCell);
                }
            }
            return positions;
        }

        public List<IElement> RestartElements()
        {
            var elements = new List<IElement>();
            foreach (var cell in Cells)
            {
                bool isNotMatch;
                var element = cell.CurrentElement;
                do
                {
                    ElementBuilder.Generate(ref element);
                    isNotMatch = CheckNotMatchFromCenter(cell);
                    if (!isNotMatch)
                    {
                        ((ElementBuilder)ElementBuilder).BunElement(cell.CurrentElement);
                    }
                } while (!isNotMatch);
                cell.SetElement(element);
                elements.Add(element);
                ((ElementBuilder)ElementBuilder).ClearBunElements();
            }
            return elements;
        }

        public bool CheckNotMatchFromCenter(Cell currentCell)
        {
            var directions = new List<Direction> { Direction.Left, Direction.Right, Direction.Up, Direction.Down };
            foreach (var direction in directions)
            {
                var aura = currentCell.CurrentElement?.Aura;
                var aura1 = currentCell.NeighborCells[direction]?.CurrentElement?.Aura;
                var aura2 = currentCell.NeighborCells[direction]?.NeighborCells[direction]?.CurrentElement?.Aura;
                if (aura == aura1 && aura == aura2)
                {
                    return false;
                }
                else
                {
                    continue;
                }
            }
            return true;
        }

        public bool CheckAnyMatch(Cell currentCell)
        {
            CheckAnyMatch(currentCell, out int countSameAuraHorizontal, out int countSameAuraVertical);
            return !(countSameAuraHorizontal < 3 && countSameAuraVertical < 3);
        }

        public bool CheckAnyMatch(Cell currentCell, out int countSameAuraHorizontal, out int countSameAuraVertical)
        {
            countSameAuraHorizontal = SumSameAura(currentCell, new List<Direction>() { Direction.Left, Direction.Right });
            countSameAuraVertical = SumSameAura(currentCell, new List<Direction>() { Direction.Up, Direction.Down });
            return !(countSameAuraHorizontal < 3 && countSameAuraVertical < 3);
        }

        public int SumSameAura(Cell currentCell, List<Direction> directions)
        {
            int countSameAura = 0;
            if (currentCell.CurrentElement?.IsLocked == true)
            {
                return countSameAura;
            }
            else
            {
                countSameAura = 1;
            }
            foreach (var direction in directions)
            {
                countSameAura += CountSameAura(direction, currentCell);
            }
            return countSameAura;
        }

        public int CountSameAura(Direction direction, Cell currentCell)
        {
            int countSameAura = 0;
            if (currentCell != null)
            {
                if (currentCell.CurrentElement?.IsLocked == true)
                {
                    return countSameAura;
                }
                bool isSameAura = currentCell.NeighborCells[direction]?.CurrentElement?.Aura == currentCell.CurrentElement?.Aura;
                bool isNotNullAura = currentCell.CurrentElement?.Aura != null;
                bool isNearbodyNotLocked = currentCell.NeighborCells[direction]?.CurrentElement?.IsLocked == false;
                if (isSameAura && isNotNullAura && isNearbodyNotLocked)
                {
                    countSameAura += CountSameAura(direction, currentCell.NeighborCells[direction]);
                    countSameAura++;
                }
            }
            return countSameAura;
        }

        public List<int> FindDestroyIndexes(Cell currentCell)
        {
            var destroyIndexes = new List<int>();
            bool isMatch = CheckAnyMatch(currentCell, out int countSameAuraHorizontal, out int countSameAuraVertical);
            if(isMatch)
            {
                destroyIndexes.Add(Cells.IndexOf(currentCell));
                FindDestroyToDirection(ref destroyIndexes, currentCell, countSameAuraHorizontal, new List<Direction> { Direction.Left, Direction.Right });
                FindDestroyToDirection(ref destroyIndexes, currentCell, countSameAuraVertical, new List<Direction>() { Direction.Up, Direction.Down });
            }
            return destroyIndexes;
        }

        public void FindDestroyToDirection(ref List<int> destroyIndexes, Cell currentCell, int countSameAuraDirection, List<Direction> directions)
        {
            if (countSameAuraDirection >= 3)
            {
                foreach (var direction in directions)
                {
                    destroyIndexes.AddRange(FindSameAura(direction, currentCell));
                }
            }
        }

        public List<int> FindSameAura(Direction direction, Cell currentCell)
        {
            List<int> cellsSameAura = new List<int>();
            if (currentCell != null)
            {
                if (currentCell.NeighborCells[direction]?.CurrentElement?.Aura == currentCell.CurrentElement?.Aura && currentCell.CurrentElement?.Aura != null)
                {
                    cellsSameAura.AddRange(FindSameAura(direction, currentCell.NeighborCells[direction]));
                    cellsSameAura.Add(Cells.IndexOf(currentCell.NeighborCells[direction]));
                }
            }
            return cellsSameAura;
        }

        public void RestoreData()
        {
            RestartElements();
        }
    }
}
