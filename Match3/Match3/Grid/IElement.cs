﻿using Microsoft.Xna.Framework;
using System;

namespace Match3
{
    public interface IElement
    {
        bool IsLocked { get; }
        Color Aura { get; }
        uint Score { get; } 

        void Initialize(Color aura, uint score);

        void SetLock(bool isLocked);
    }
}