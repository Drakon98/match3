﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class Element : IElement
    {
        public bool IsLocked { get; private set; }
        public Color Aura { get; private set; }
        public uint Score { get; private set; }

        public Element(Color aura, uint score)
        {
            Initialize(aura, score);
        }

        public void Initialize(Color aura, uint score)
        {
            Aura = aura;
            Score = score;
        }

        public void SetLock(bool isLocked) => IsLocked = isLocked;

        public override string ToString() => $"{Aura} {Score}";
    }
}
