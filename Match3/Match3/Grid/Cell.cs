﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public enum Direction { Left, Right, Up, Down };
    public class Cell
    {
        public Dictionary<Direction, Cell> NeighborCells { get; private set; } = new Dictionary<Direction, Cell>()
        { { Direction.Left, null }, { Direction.Right, null }, { Direction.Up, null }, { Direction.Down, null } };

        public IElement CurrentElement { get; private set; }

        public Cell() { }

        public void CreateNearbody(Cell left, Cell right, Cell up, Cell down)
        {
            NeighborCells[Direction.Left] = left;
            NeighborCells[Direction.Right] = right;
            NeighborCells[Direction.Up] = up;
            NeighborCells[Direction.Down] = down;
        }

        public void SetElement(IElement element) => CurrentElement = element;

        public override string ToString() => $"{CurrentElement}";
    }
}
