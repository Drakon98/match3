﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class DestroyEventArgs : EventArgs
    {
        public uint Score { get; private set; }

        public DestroyEventArgs(uint score) => Score = score;
    }
}
