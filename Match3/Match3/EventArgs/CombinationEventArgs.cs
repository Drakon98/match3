﻿using Microsoft.Xna.Framework;
using System;

namespace Match3
{
    public class CombinationEventArgs : EventArgs
    {
        public PairElements PairElements { get; private set; }

        public Point FirstCountSameAura { get; private set; }

        public Point SecondCountSameAura { get; private set; }

        public CombinationEventArgs(PairElements pairElements, Point firstCountSameAura, Point secondCountSameAura)
        {
            PairElements = pairElements;
            FirstCountSameAura = firstCountSameAura;
            SecondCountSameAura = secondCountSameAura;
        }
    }
}