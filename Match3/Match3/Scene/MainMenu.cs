﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class MainMenu : IDataComponent
    {
        public MainMenuPanel MainMenuPanel { get; private set; }

        public MainMenu()
        {
            MainMenuPanel = new MainMenuPanel();
            MainMenuPanel.PlayButton.Click += PlayButtonClick;
        }

        public List<IComponent> GetComponents() => MainMenuPanel.GetComponents();

        public void PlayButtonClick(object sender, EventArgs eventArgs) => Application.ChangeScene("Level1");

        public void RestoreData() => MainMenuPanel.RestoreData();
    }
}
