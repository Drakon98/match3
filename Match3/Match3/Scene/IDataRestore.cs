﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public interface IDataRestore
    {
        void RestoreData();
    }
}
