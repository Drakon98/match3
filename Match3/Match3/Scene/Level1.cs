﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class Level1 : IDataComponent
    {
        public MainPanel MainPanel { get; private set; }
        public ResultPanel ResultPanel { get; private set; }
        public GridPanel GridPanel { get; private set; }
        public Timer Timer { get; private set; }
        public Score Score { get; private set; }

        public Level1()
        {
            MainPanel = new MainPanel();
            MainPanel.TimerText.TextChanged += TimerUpdate;
            MainPanel.ScoreText.TextChanged += ScoreUpdate;
            ResultPanel = new ResultPanel();
            ResultPanel.GameOverText.Activated += delegate (object sender, EventArgs eventArgs)
            {
                MainPanel.TimerText.SetActive(false);
                GridPanel.GridGameObject.SetActive(false);
            };
            ResultPanel.AcceptButton.Click += ReturnMainMenuClick;
            GridPanel = new GridPanel();
            GridPanel.GridGameObject.DestroyedElement += GridGameObjectDestroyElements;
            Timer = new Timer();
            Timer.Timeout += TimerTimeout;
            Score = new Score();
        }

        public List<IComponent> GetComponents()
        {
            var components = new List<IComponent>();
            components.AddRange(MainPanel.GetComponents());
            components.AddRange(GridPanel.GetComponents());
            components.AddRange(ResultPanel.GetComponents());
            return components;
        }

        public void RestoreData()
        {
            Timer.RestoreData();
            Score.RestoreData();
            MainPanel.RestoreData();
            ResultPanel.RestoreData();
            GridPanel.RestoreData();
        }

        public void TimerUpdate(object sender, EventArgs eventArgs)
        {
            MainPanel.TimerText.Value = $"Time: {Timer.CurrentTime:0}";
            Timer.Run();
        }

        public void TimerTimeout(object sender, EventArgs eventArgs)
        {
            MainPanel.TimerText.TextChanged -= TimerUpdate;
            MainPanel.ScoreText.TextChanged -= ScoreUpdate;
            ResultPanel.GameOverText.SetActive(true);
            ResultPanel.AcceptButton.SetActive(true);
        }

        public void GridGameObjectDestroyElements(object sender, DestroyEventArgs destroyEventArgs) => Score.AddScore(destroyEventArgs.Score);

        public void ScoreUpdate(object sender, EventArgs eventArgs)
        {
            MainPanel.ScoreText.Value = $"Score: {Score.Value}";
        }

        public void ReturnMainMenuClick(object sender, EventArgs eventArgs)
        {
            MainPanel.TimerText.TextChanged += TimerUpdate;
            MainPanel.ScoreText.TextChanged += ScoreUpdate;
            Application.ChangeScene("MainMenu");
        }
    }
}
