﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Match3
{
    public class Text : GameObject
    {
        private string fontName;

        public Vector2 Position { get; set; }

        public SpriteFont SpriteFont { get; set; }

        public string Value { get; set; }

        public event EventHandler TextChanged;

        public Text(string fontName, string value, Vector2 position, Color color)
        {
            this.fontName = fontName;
            Value = value;
            Position = position;
            currentColor = color;
            lastColor = color;
        }

        public Text(string fontName, string value)
        {
            this.fontName = fontName;
            Value = value;
            Position = Vector2.Zero;
            currentColor = Color.Black;
            lastColor = Color.Black;
        }

        public override void Initialize() { }

        public override void LoadContent(ContentManager content) => SpriteFont = content.Load<SpriteFont>(fontName);

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (IsActive)
            {
                spriteBatch.DrawString(SpriteFont, Value, Position, currentColor);
            }
        }

        public override void Update(GameTime gameTime)
        {
            if (IsActive)
            {
                TextChanged?.Invoke(this, new EventArgs());
            }
        }

        public override string ToString() => Value;

        public override void UnloadContent(ContentManager content) => content.Unload();

        public override void SetActive(bool isActive) => base.SetActive(isActive);
    }
}
