﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class ElementGameObject : GameObject
    {
        private bool isFirstClick = true;
        public Vector2 Position { get; set; }

        public Color ActivatedColor { get; private set; }

        public float Speed { get; private set; }

        public IElement Element { get; private set; }

        public Button Button { get; private set; }

        public event EventHandler FirstClick, SecondClick;

        public ElementGameObject(IElement element, Button button, Vector2 position, Color activatedColor, float speed = 0.03f)
        {
            Element = element;
            Button = button;
            Position = position;
            ActivatedColor = activatedColor;
            Speed = speed;
        }

        public void SetElement(IElement element) => Element = element;

        public override void Initialize()
        {
            Button.NormalColor = Element.Aura;
            Button.HoveringColor = Color.Aqua;
            Button.Position = Position;
            Button.Initialize();
            Button.Click += ButtonClick;
        }

        public override void LoadContent(ContentManager content) => Button.LoadContent(content);

        public override void Update(GameTime gameTime)
        {
            if (IsActive)
            {
                Button.Update(gameTime);
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (IsActive)
            {
                Button.Draw(gameTime, spriteBatch);
            }
        }

        public override void UnloadContent(ContentManager content) => Button.UnloadContent(content);

        public override void SetActive(bool isActive)
        {
            IsActive = isActive;
            Button.SetActive(isActive);
        }

        public void ButtonClick(object sender, EventArgs eventArgs)
        {
            if (isFirstClick)
            {
                Button.NormalColor = ActivatedColor;
                isFirstClick = false;
                FirstClick?.Invoke(this, new EventArgs());
            }
            else
            {
                ClearHighlight();
                SecondClick?.Invoke(this, new EventArgs());
            }            
        }

        public void ClearHighlight()
        {
            Button.NormalColor = Element.Aura;
            isFirstClick = true;
        }

        public uint GetScore() => Element.Score;

        public bool Move(Vector2 start, Vector2 target)
        {
            bool isMoved = false;
            var velocity = new Vector2(target.X - start.X, target.Y - start.Y) * Speed;
            Position += velocity;
            Button.Position += velocity;
            bool isEnterX = start.X >= Position.X && Position.X >= target.X || start.X <= Position.X && Position.X <= target.X;
            bool isEnterY = start.Y >= Position.Y && Position.Y >= target.Y || start.Y <= Position.Y && Position.Y <= target.Y;
            if (Position == target)
            {
                isMoved = true;
            }
            if (!isEnterX || !isEnterY)
            {
                Position = target;
                Button.Position = target;
                isMoved = true;
            }
            return isMoved;
        }

        public override string ToString() => $"{Position} {Element.Aura} {Element.Score}";
    }
}
