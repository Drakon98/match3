﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class Button : GameObject
    {
        private MouseState previousState;

        private MouseState currentState;

        private bool isHovering;

        public Vector2 Position { get; set; }

        public Color NormalColor { get; set; }

        public Color HoveringColor { get; set; } = Color.Gray;

        public Text Text { get; set; }

        public Image Image { get; set; }

        public event EventHandler Click;

        public Button(Image image, Text text, Vector2 position, Color color)
        {
            Image = image;
            Text = text;
            Position = position;
            Color = color;
            NormalColor = color;
            lastColor = color;
        }

        public Button(Image image, Text text)
        {
            Image = image;
            Text = text;
            Position = Vector2.Zero;
            Color = Color.White;
            NormalColor = Color.White;
            lastColor = Color.White;
        }
        public override void Initialize()
        {
            Image.Position = Position;
        }

        public override void LoadContent(ContentManager content)
        {
            Image.LoadContent(content);
            Text.LoadContent(content);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (IsActive)
            {
                currentColor = NormalColor;
                if (isHovering)
                {
                    currentColor = HoveringColor;
                }
                spriteBatch.Draw(Image.Texture, Position, currentColor);
                if (!string.IsNullOrEmpty(Text.Value))
                {
                    spriteBatch.DrawString(Text.SpriteFont, Text.Value, new Vector2(Image.Texture.Width / 2, Image.Texture.Height / 2) + Position - Text.SpriteFont.MeasureString(Text.Value) / 2, Text.Color);
                }
            }
        }
        public override void Update(GameTime gameTime)
        {
            if (IsActive)
            {
                previousState = currentState;
                currentState = Mouse.GetState();

                isHovering = false;
                Image.Position = Position;
                if (Image.Rectangle.Intersects(new Rectangle(currentState.X, currentState.Y, 1, 1)))
                {
                    isHovering = true;
                    if (previousState.LeftButton == ButtonState.Pressed && currentState.LeftButton == ButtonState.Released)
                    {
                        Click?.Invoke(this, new EventArgs());
                    }
                }
            }
        }
        public override void UnloadContent(ContentManager content)
        {
            Image.UnloadContent(content);
            Text.UnloadContent(content);
            content.Unload();
        }

        public override void SetActive(bool isActive)
        {
            base.SetActive(isActive);
            Text.SetActive(isActive);
            Image.SetActive(isActive);
        }
    }
}
