﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class GridGameObject : GameObject
    {
        public Vector2 Position { get; set; }
        public List<IElement> ExistElements { get; private set; } = new List<IElement>();
        public Grid Grid { get; private set; }
        public ResourceLoader<Image> ResourceLoader { get; private set; }
        public Button Button { get; private set; }
        public CombinationDetector DeterminantCombinations { get; private set; }

        public RespawnerElements RespawnerElements { get; private set; }

        public event EventHandler<DestroyEventArgs> DestroyedElement;

        public List<CellGameObject> Cells { get; private set; } = new List<CellGameObject>();

        public GridGameObject(List<IElement> existElements, ResourceLoader<Image> resourceLoader, Vector2 position)
        {
            ExistElements = existElements;
            ResourceLoader = resourceLoader;
            Position = position;
        }

        public override void Initialize()
        {
            Grid = new Grid(new ElementBuilder(ExistElements), new Point(8, 8));
            Cells = new List<CellGameObject>();
        }

        public override void LoadContent(ContentManager content)
        {
            ResourceLoader.LoadContent(content);
            var line = CloneResource("Line");
            var bomb = CloneResource("Bomb");

            foreach (var cell in Grid.Cells)
            {
                var cellImage = new Image(ResourceLoader.Resources["Cell"].TextureName, Vector2.Zero, ResourceLoader.Resources["Cell"].Color);
                Button = new Button(new Image(ResourceLoader.Resources["Element"].TextureName), new Text("Font", string.Empty));
                var element = new ElementGameObject(Grid.ElementBuilder.Create(), Button, Vector2.Zero, Color.Lime);
                var destroyers = new Dictionary<Direction, Image>() 
                { 
                    { Direction.Left, CloneResource("DestroyerLeft") },
                    { Direction.Right, CloneResource("DestroyerRight") },
                    { Direction.Up, CloneResource("DestroyerUp") },
                    { Direction.Down, CloneResource("DestroyerDown") }
                };
                var destroyerBuilder = new DestroyerBuilder(destroyers, CloneResource("Explosian"));
                Cells.Add(new CellGameObject(cell, cellImage, element, Vector2.Zero, destroyerBuilder));
                Cells[Cells.Count - 1].DestroyedElement += DestroyedElement;
            }
            Button.LoadContent(content);
            CreateGrid();
            RespawnerElements = new RespawnerElements(Cells, Grid, new ElementBuilder(ExistElements), Button, Color.Lime);
            RespawnerElements.CheckedCombination += CheckedCombination;
            RespawnerElements.Initialize();

            DeterminantCombinations = new CombinationDetector(line, bomb, RespawnerElements);
            RespawnerElements.CreateGrid(ResourceLoader.Resources["Cell"].Texture.Width, ResourceLoader.Resources["Cell"].Texture.Height, Position);

            for (int i = 0; i < Cells.Count; i++)
                Cells[i].LoadContent(content);
        }

        public override void Update(GameTime gameTime)
        {
            if (IsActive)
            {
                foreach (var cell in Cells)
                {
                    cell.Update(gameTime);
                }
                RespawnerElements.UpdateMoveElements();
                UpdateDestroyElements();
            }
        }

        private Image CloneResource(string resourceName)
        {
            var resource = ResourceLoader.Resources[resourceName];
            return new Image(resource.TextureName, Vector2.Zero, resource.Color) { Texture = resource.Texture };
        }

        public void UpdateDestroyElements()
        {
            var destroyIndexes = new List<int>();
            foreach (var cell in Grid.Cells)
            {
                if (cell.CurrentElement != null)
                {
                    bool isMatch = Grid.CheckAnyMatch(cell);
                    if (isMatch)
                    {
                        destroyIndexes.AddRange(Grid.FindDestroyIndexes(cell));
                    }
                }
            }

            List<int> destroyUniqueIndexes = destroyIndexes.Distinct().ToList();
            foreach (var destroyIndex in destroyUniqueIndexes)
            {
                Cells[destroyIndex].DestroyElement();
            }
        }

        public void DestroyCombination(bool isHaveCombination, ElementGameObject element, int index, int countSameAuraHorizontal, int countSameAuraVertical)
        {
            if (isHaveCombination)
            {
                DestroyElements(Cells[index].Cell);
                DeterminantCombinations.GetCombinations(element, index, countSameAuraHorizontal, countSameAuraVertical);
            }
        }

        public void DestroyElements(Cell currentCell)
        {
            var destroyIndexes = new List<int>();
            destroyIndexes.AddRange(Grid.FindDestroyIndexes(currentCell));
            foreach (var destroyIndex in destroyIndexes)
            {
                if (destroyIndex != -1)
                {
                    Cells[destroyIndex].DestroyElement();
                }
            }
        }

        public void CheckedCombination(object sender, CombinationEventArgs combinationEventArgs)
        {
            var firstCountSameAura = combinationEventArgs.FirstCountSameAura;
            var secondCountSameAura = combinationEventArgs.SecondCountSameAura;
            bool isHaveFirstCombination = DeterminantCombinations.ExistCombinations(firstCountSameAura.X, firstCountSameAura.Y);
            bool isHaveSecondCombination = DeterminantCombinations.ExistCombinations(secondCountSameAura.X, secondCountSameAura.Y);
            var pairElements = combinationEventArgs.PairElements;
            DestroyCombination(isHaveFirstCombination, pairElements.SecondElement, pairElements.FirstIndex, firstCountSameAura.X, firstCountSameAura.Y);
            DestroyCombination(isHaveSecondCombination, pairElements.FirstElement, pairElements.SecondIndex, secondCountSameAura.X, secondCountSameAura.Y);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (IsActive)
            {
                foreach (var cell in Cells)
                {
                    cell.DrawCell(gameTime, spriteBatch);
                }
                foreach (var cell in Cells)
                {
                    cell.DrawElement(gameTime, spriteBatch);
                }
                foreach (var cell in Cells)
                {
                    cell.DrawDestroyer(gameTime, spriteBatch);
                }
            }
        }

        public override void UnloadContent(ContentManager content)
        {
            foreach (var cell in Cells)
            {
                cell.UnloadContent(content);
            }
        }

        public override void SetActive(bool isActive)
        {
            IsActive = isActive;
            foreach (var cell in Cells)
            {
                cell.SetActive(isActive);
            }
        }

        private void CreateGrid()
        {
            for (int i = 0; i < Grid.Size.X; i++)
            {
                for (int j = 0; j < Grid.Size.Y; j++)
                {
                    var x = i * Grid.Size.X + j;
                    var leftCell = j == 0 ? null : Cells[x - 1];
                    var rightCell = j == Grid.Size.X - 1 ? null : Cells[x + 1];
                    var upCell = i == 0 ? null : Cells[(i - 1) * Grid.Size.X + j];
                    var downCell = i == Grid.Size.Y - 1 ? null : Cells[(i + 1) * Grid.Size.X + j];
                    Cells[x].CreateNearbody(leftCell, rightCell, upCell, downCell);
                }
            }
        }
    }
}
