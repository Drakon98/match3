﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{

    public class Image : GameObject
    {
        public string TextureName { get; }

        public Vector2 Position { get; set; }

        public Texture2D Texture { get; set; }

        public Rectangle Rectangle
        {
            get
            {
                var textureWidth = Texture != null ? Texture.Width : 0;
                var textureHeight = Texture != null ? Texture.Height : 0;
                return new Rectangle((int)Position.X, (int)Position.Y, textureWidth, textureHeight);
            }
        }

        public Image(string textureName, Vector2 position, Color color)
        {
            TextureName = textureName;
            Position = position;
            lastColor = color;
            currentColor = color;
        }

        public Image(string textureName)
        {
            TextureName = textureName;
            Position = Vector2.Zero;
            lastColor = Color.White;
            currentColor = Color.White;
        }
        public override void Initialize() { }
        
        public override void LoadContent(ContentManager content) => Texture = content.Load<Texture2D>(TextureName);

        public override void Update(GameTime gameTime) { }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (IsActive)
            {
                spriteBatch.Draw(Texture, Rectangle, Color);
            }
        }

        public override void UnloadContent(ContentManager content)
        {
            Texture.Dispose();
            content.Unload();
        }

        public override void SetActive(bool isActive) => base.SetActive(isActive);
    }
}
