﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class CellGameObject : GameObject
    {
        public Vector2 Position { get; set; }

        public Cell Cell { get; private set; }

        public Image Image { get; private set; }

        public ElementGameObject Element { get; private set; }

        public List<IDestroyer> Destroyers { get; private set; } = new List<IDestroyer>();

        public DestroyerBuilder DestroyerBuilder { get; private set; }

        public Dictionary<Direction, CellGameObject> NeighborCells { get; private set; } = new Dictionary<Direction, CellGameObject>()
        { { Direction.Left, null }, { Direction.Right, null }, { Direction.Up, null }, { Direction.Down, null } };

        public event EventHandler<DestroyEventArgs> DestroyedElement;

        public CellGameObject(Cell cell, Image image, ElementGameObject element, Vector2 position, DestroyerBuilder destroyerBuilder)
        {
            Cell = cell;
            Image = image;
            Element = element;
            Position = position;
            DestroyerBuilder = destroyerBuilder;
        }

        public void CreateNearbody(CellGameObject left, CellGameObject right, CellGameObject up, CellGameObject down)
        {
            NeighborCells[Direction.Left] = left;
            NeighborCells[Direction.Right] = right;
            NeighborCells[Direction.Up] = up;
            NeighborCells[Direction.Down] = down;
        }

        public void ChangeElementGameObject(ElementGameObject elementGameObject)
        {
            Element = elementGameObject;
            Cell.SetElement(elementGameObject?.Element);
        }

        public void ChangeElement(IElement element)
        {
            Cell.SetElement(element);
            Element.SetElement(element);
        }

        public List<Cell> GetNearbodyCells()
        {
            var cells = new List<Cell>
            {
                Cell.NeighborCells[Direction.Left],
                Cell.NeighborCells[Direction.Right],
                Cell.NeighborCells[Direction.Up],
                Cell.NeighborCells[Direction.Down]
            };
            return cells;
        }

        public void DestroyElement()
        {
            if (Element?.Element.Score > 1)
            {
                Destroyers.AddRange(DestroyerBuilder.CreateDestroyer(Element.Element, this));
            }
            if (Element != null)
            {
                DestroyedElement?.Invoke(this, new DestroyEventArgs(Element.Element.Score));
            }
            ChangeElementGameObject(null);
        }

        public override void Initialize()
        {
            Image.Position = Position;
            Element.Position = Position;
            Element.Initialize();
        }

        public override void LoadContent(ContentManager content)
        {
            Image.LoadContent(content);
            Element.LoadContent(content);
        }

        public override void Update(GameTime gameTime)
        {
            Image.Update(gameTime);
            Element?.Update(gameTime);
            for(int i = 0; i < Destroyers.Count; i++)
            {
                ((GameObject)Destroyers[i]).Update(gameTime);
                if (Destroyers[i].IsStop)
                {
                    Destroyers.Remove(Destroyers[i]);
                    i--;
                }
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            DrawCell(gameTime, spriteBatch);
            DrawElement(gameTime, spriteBatch);
            DrawDestroyer(gameTime, spriteBatch);
        }

        public void DrawCell(GameTime gameTime, SpriteBatch spriteBatch) => Image.Draw(gameTime, spriteBatch);

        public void DrawElement(GameTime gameTime, SpriteBatch spriteBatch) => Element?.Draw(gameTime, spriteBatch);

        public void DrawDestroyer(GameTime gameTime, SpriteBatch spriteBatch)
        {
            foreach (var destroyer in Destroyers)
            {
                ((GameObject)destroyer)?.Draw(gameTime, spriteBatch);
            }
        }

        public override void UnloadContent(ContentManager content)
        {
            Image.UnloadContent(content);
            Element?.UnloadContent(content);
        }

        public override void SetActive(bool isActive)
        {
            Image.SetActive(isActive);
            Element?.SetActive(isActive);
        }

        public override string ToString() => $"{Position} {Element}";
    }
}
