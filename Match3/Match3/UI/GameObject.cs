﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class GameObject : IComponent
    {
        protected Color lastColor;

        protected Color currentColor;

        public Color Color { get => currentColor; set { lastColor = Color; currentColor = value; } }

        public bool IsActive { get; protected set; } = true;

        public event EventHandler Activated;
        public event EventHandler Disactivated;

        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch) { }

        public virtual void Initialize() { }

        public virtual void LoadContent(ContentManager content) { }

        public virtual void SetActive(bool isActive)
        {
            IsActive = isActive;
            if (IsActive)
            {
                currentColor = lastColor;
                Activated?.Invoke(this, new EventArgs());
            }
            else
            {
                if (currentColor != Color.Transparent)
                {
                    lastColor = currentColor;
                }
                currentColor = Color.Transparent;     
                Disactivated?.Invoke(this, new EventArgs());
            }
        }

        public virtual void UnloadContent(ContentManager content) { }

        public virtual void Update(GameTime gameTime) { }
    }
}
