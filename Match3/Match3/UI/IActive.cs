﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public interface IActive
    {
        bool IsActive { get; }

        event EventHandler Activated;

        event EventHandler Disactivated;

        void SetActive(bool isActive);
    }
}
