﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Match3
{
    public interface IComponent : IActive
    {
        void Initialize();

        void LoadContent(ContentManager content);

        void Update(GameTime gameTime);

        void Draw(GameTime gameTime, SpriteBatch spriteBatch);

        void UnloadContent(ContentManager content);
    }
}
