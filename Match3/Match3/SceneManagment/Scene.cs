﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class Scene
    {
        public List<IComponent> Components { get; set; } = new List<IComponent>();
        public IDataComponent DataComponent { get; }

        public string Name { get; }

        public Scene(string name, IDataComponent dataComponent)
        {
            Name = name;
            DataComponent = dataComponent;
        }

        public void Load()
        {
            Components = new List<IComponent>();
            Components = DataComponent.GetComponents();
        }
    }
}
