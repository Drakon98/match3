﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;

namespace Match3
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Application : Game
    {
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private static readonly List<Scene> scenes = new List<Scene>();
        private static Scene currentScene;
        private static bool isSceneChanged;
        private static bool isSceneRestore;
        
        public Application(List<Scene> existScenes)
        {
            scenes.AddRange(existScenes);
            currentScene = scenes[0];
            currentScene.Load();
            graphics = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferWidth = 1920,
                PreferredBackBufferHeight = 1020,
                //IsFullScreen = true
            };
            Content.RootDirectory = "Content";
        }

        public static bool ChangeScene(string sceneName, bool isDataRestore = true)
        {
            var nextScene = scenes.Find(scene => scene.Name == sceneName);
            var sceneIndex = scenes.IndexOf(nextScene);
            if (sceneIndex != -1)
            {
                currentScene = scenes[sceneIndex];
                currentScene.Load();
                isSceneChanged = true;
                isSceneRestore = isDataRestore;
                return true;
            }
            return false;
        }
        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            IsMouseVisible = true;

            foreach (var component in currentScene.Components)
            {
                component.Initialize();
            }
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            foreach (var component in currentScene.Components)
            {
                component.LoadContent(Content);
            }
        }
        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            foreach (var component in currentScene.Components)
            {
                component.UnloadContent(Content);
            }
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            foreach (var component in currentScene.Components)
            {
                component.Update(gameTime);
            }
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            if (isSceneChanged)
            {
                foreach (var component in currentScene.Components)
                {
                    component.Initialize();
                    component.LoadContent(Content);
                }
                if (isSceneRestore)
                {
                    currentScene.DataComponent.RestoreData();
                }
                isSceneChanged = false;
            }
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();
            foreach (var component in currentScene.Components)
            {
                component.Draw(gameTime, spriteBatch);
            }
            spriteBatch.End();
            
            base.Draw(gameTime);
        }
    }
}
