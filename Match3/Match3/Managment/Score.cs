﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class Score : IDataRestore
    {
        public uint StartValue { get; private set; }
        public uint Value { get; private set; }

        public Score(uint startValue = 0)
        {
            StartValue = startValue;
            Value = StartValue;
        }

        public void AddScore(uint score) => Value += score;

        public void ChangeStartValue(uint startValue) => StartValue = startValue;

        public void RestoreData() => Value = StartValue;
    }
}
