﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class ResourceLoader<T> where T: GameObject
    {
        public Dictionary<string, T> Resources { get; private set; } = new Dictionary<string, T>();

        public ResourceLoader(Dictionary<string, T> resources) => Resources = resources;

        public void LoadContent(ContentManager content)
        {
            foreach (var resources in Resources)
            {
                resources.Value.LoadContent(content);
            }
        }

        public void Initialize()
        {
            foreach (var resources in Resources)
            {
                resources.Value.Initialize();
            }
        }
    }
}
