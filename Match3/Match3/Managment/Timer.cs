﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class Timer : IDataRestore
    {
        private double totalTime;
        public double TotalTime { get => totalTime; private set => totalTime = Math.Abs(value); }

        private double currentTime;
        public double CurrentTime { get => currentTime; private set { if (value >= 0) currentTime = value; else currentTime = 0; } }
        public bool IsStart { get; private set; }
        public Stopwatch Stopwatch { get; private set; } = new Stopwatch();

        public event EventHandler Timeout;

        public Timer(double totalTime = 60)
        {
            TotalTime = totalTime;
            CurrentTime = totalTime;
        }

        public void RestoreData()
        {
            CurrentTime = TotalTime;
            Stopwatch.Reset();
            IsStart = false;
        }

        public void Run()
        {
            if (!IsStart)
            {
                Stopwatch.Start();
                IsStart = true;
            }
            var timeSpan = Stopwatch.Elapsed;
            CurrentTime = TotalTime - (int)timeSpan.TotalSeconds;
            if (CurrentTime == 0)
            {
                Stopwatch.Stop();
                Timeout?.Invoke(this, new EventArgs());
            }
        }

        public void ChangeTotalTime(int totalTime) => TotalTime = totalTime;

        public void AddTotalTime(int addTime) => TotalTime += addTime;

        public void AddCurrentTime(int addTime) => CurrentTime += addTime;

    }
}
