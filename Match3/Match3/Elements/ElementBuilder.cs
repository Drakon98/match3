﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class ElementBuilder : IElementBuilder
    {
        public List<IElement> ExistElements { get; private set; } = new List<IElement>();
        public List<IElement> DublicateElements { get; private set; }
        public Random Random { get; private set; }

        public ElementBuilder(List<IElement> existElements)
        {
            ExistElements = existElements;
            ClearBunElements();
            Random = new Random();
        }

        public IElement Create() => new Element(Color.White, 1);

        public void Generate(ref IElement element)
        {
            var index = Random.Next(DublicateElements.Count);
            element.Initialize(DublicateElements[index].Aura, DublicateElements[index].Score);
        }
        public void BunElement(IElement element) => DublicateElements.Remove(DublicateElements.First(dublicate => dublicate.Aura == element.Aura));
        public void ClearBunElements()
        {
            DublicateElements = new List<IElement>();
            DublicateElements.AddRange(ExistElements);
        }
    }
}
