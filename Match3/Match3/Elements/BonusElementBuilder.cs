﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public enum Bonus { Line, Bomb }
    public class BonusElementBuilder : IElementBuilder
    {
        public IElement Line { get; private set; }

        public IElement Bomb { get; private set; }

        public Bonus CurrentBonus { get; private set; }

        public Color CurrentColor { get; private set; }

        public Random Random { get; private set; } = new Random();

        public BonusElementBuilder(IElement line, IElement bomb)
        {
            Line = line;
            Bomb = bomb;
        }

        public IElement Create() => new Element(Color.White, 1);

        public void Generate(ref IElement element)
        {
            switch (CurrentBonus)
            {
                case Bonus.Line: element = new Line(CurrentColor, Line.Score, (Axis)Random.Next(2)); break;
                case Bonus.Bomb: element = new Bomb(CurrentColor, Bomb.Score); break;
            }
        }

        public void Generate(ref IElement element, Bonus bonus, Color color)
        {
            CurrentBonus = bonus;
            CurrentColor = color;
            Generate(ref element);
        }

        
    }
}
