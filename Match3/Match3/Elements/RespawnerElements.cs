﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class RespawnerElements
    {
        private ElementGameObject firstElement, secondElement;
        private int firstIndex, secondIndex;
        private bool isMoving = false;
        public List<CellGameObject> Cells { get; private set; } = new List<CellGameObject>();

        public Grid Grid { get; private set; }

        public IElementBuilder ElementBuilder { get; private set; }

        public Button Button { get; private set; }

        public Color ActivatedColor { get; private set; }

        public List<PairElements> UserPairsElements { get; private set; } = new List<PairElements>();

        public List<PairElements> RespawnPairElements { get; private set; } = new List<PairElements>();

        public EventHandler<CombinationEventArgs> CheckedCombination;

        public RespawnerElements(List<CellGameObject> cells, Grid grid, IElementBuilder elementBuilder, Button button, Color activatedColor)
        {
            Cells = cells;
            Grid = grid;
            ElementBuilder = elementBuilder;
            Button = button;
            ActivatedColor = activatedColor;
        }

        public void Initialize()
        {
            firstElement = null;
            secondElement = null;
        }

        public void CheckCells()
        {
            for (int i = 0; i < Grid.Size.X; i++)
            {
                if (Cells[i].Cell.CurrentElement == null)
                {
                    CreateElement(i);
                }
            }
            if (RespawnPairElements.Count == 0)
            {
                for (int i = 0; i < Grid.Size.X; i++)
                {
                    CheckColumn(Cells[Grid.Size.X * Grid.Size.Y - Grid.Size.Y + i].Cell, i, false);
                }
            }
        }

        private void CheckColumn(Cell lastColumnElement, int column, bool isNullColumn)
        {
            if(lastColumnElement.CurrentElement == null && lastColumnElement.NeighborCells[Direction.Up] != null || isNullColumn && Grid.Cells.IndexOf(lastColumnElement) != column)
            {
                int firstIndex = Grid.Cells.IndexOf(lastColumnElement);
                int secondIndex = Grid.Cells.IndexOf(lastColumnElement.NeighborCells[Direction.Up]);
                var pairElements = new PairElements(Cells[firstIndex].Element, Cells[secondIndex].Element, firstIndex, secondIndex, false, true);
                if (!RespawnPairElements.Any(pair => pair.FirstIndex == pairElements.FirstIndex && pair.SecondIndex == pairElements.SecondIndex))
                {
                    RespawnPairElements.Add(pairElements);
                    Grid.Cells[firstIndex].CurrentElement?.SetLock(true);
                    Grid.Cells[secondIndex].CurrentElement?.SetLock(true);
                }
                CheckColumn(lastColumnElement.NeighborCells[Direction.Up], column, true);
            }
            else
            {
                if (Grid.Cells.IndexOf(lastColumnElement) != column)
                {
                    CheckColumn(lastColumnElement.NeighborCells[Direction.Up], column, false);
                }
            }
        }

        public void CreateGrid(int cellWidth, int cellHeight, Vector2 position)
        {
            var positionsCells = Grid.Build(cellWidth, cellHeight, position);
            var elements = Grid.RestartElements();
            for (int i = 0; i < Cells.Count; i++)
            {
                Cells[i].Position = positionsCells[i];
                Cells[i].ChangeElement(elements[i]);
                Cells[i].Initialize();
                Cells[i].Element.FirstClick += FirstClick;
                Cells[i].Element.SecondClick += SecondClick;
            }
        }

        public void CreateElement(int index)
        {
            var element = ElementBuilder.Create();
            ElementBuilder.Generate(ref element);
            var button = new Button(new Image(Button.Image.TextureName) { Texture = Button.Image.Texture }, Button.Text);
            Cells[index].ChangeElementGameObject(new ElementGameObject(element, button, Cells[index].Position, ActivatedColor));
            Cells[index].Initialize();
            Cells[index].Element.FirstClick += FirstClick;
            Cells[index].Element.SecondClick += SecondClick;
        }

        public void CreateBonus(Button button, IElement element, int index)
        {
            Cells[index].ChangeElementGameObject(new ElementGameObject(element, button, Cells[index].Position, ActivatedColor));
            Cells[index].Initialize();
            Cells[index].Element.FirstClick += FirstClick;
            Cells[index].Element.SecondClick += SecondClick;
        }

        public void UpdateMoveElements()
        {
            UserMoveElements();
            RespawnMoveElements();
        }

        private void RespawnMoveElements()
        {
            if (Cells.Any(cell => cell.Element == null))
            {
                CheckCells();
                RespawnPairElements = RespawnPairElements.OrderByDescending(pair => pair.FirstIndex).ToList();
            }
            ClearEmptyPair();
            for (int i = 0; i < RespawnPairElements.Count; i++)
            {
                MoveElement(RespawnPairElements[i], RespawnPairElements[i].SecondIndex, RespawnPairElements[i].FirstIndex);
                SwapElements(RespawnPairElements[i], RespawnPairElements[i].FirstElement, RespawnPairElements[i].SecondElement);
                i = RestoreRespawnPairElements(i);
            }
        }

        private int RestoreRespawnPairElements(int i)
        {
            if (!RespawnPairElements[i].IsMoving)
            {
                Cells[RespawnPairElements[i].SecondIndex].ChangeElementGameObject(null);
                bool isNullBelow = CheckNullBelow(i);
                if (!isNullBelow)
                {
                    Grid.Cells[RespawnPairElements[i].FirstIndex].CurrentElement?.SetLock(false);
                    Grid.Cells[RespawnPairElements[i].SecondIndex].CurrentElement?.SetLock(false);
                    CheckMatch(RespawnPairElements, i);
                }
                i = RemoveRespawnPairElements(i);
            }

            return i;
        }

        private void ClearEmptyPair()
        {
            for (int i = 0; i < RespawnPairElements.Count; i++)
            {
                bool isFirstNull = Cells[RespawnPairElements[i].FirstIndex].Element == null;
                bool isSecondNull = Cells[RespawnPairElements[i].SecondIndex].Element == null;
                if (isFirstNull && isSecondNull)
                {
                    i = RemoveRespawnPairElements(i);
                    continue;
                }
            }
        }

        private int RemoveRespawnPairElements(int i)
        {
            RespawnPairElements.RemoveAt(i);
            i--;
            if (i == RespawnPairElements.Count - 1)
            {
                RespawnPairElements.Clear();
            }
            return i;
        }

        private bool CheckNullBelow(int i)
        {
            int column = RespawnPairElements[i].FirstIndex % Grid.Size.X;
            bool isNullBelow = false;
            for (int y = 0; y < Grid.Size.Y; y++)
            {
                int index = column + y * Grid.Size.X;
                if (index > RespawnPairElements[i].FirstIndex)
                {
                    if (Grid.Cells[index].CurrentElement == null)
                    {
                        isNullBelow = true;
                        break;
                    }
                }
            }
            return isNullBelow;
        }

        private void UserMoveElements()
        {
            for (int i = 0; i < UserPairsElements.Count; i++)
            {
                if (!UserPairsElements[i].IsReturn)
                {
                    MoveElements(UserPairsElements[i], UserPairsElements[i].FirstIndex, UserPairsElements[i].SecondIndex);
                    SwapElements(UserPairsElements[i], UserPairsElements[i].FirstElement, UserPairsElements[i].SecondElement);
                }
                CheckMatch(i);
                if (i >= UserPairsElements.Count)
                {
                    continue;
                }
                if (UserPairsElements[i].IsReturn)
                {
                    MoveElements(UserPairsElements[i], UserPairsElements[i].SecondIndex, UserPairsElements[i].FirstIndex);
                    SwapElements(UserPairsElements[i], UserPairsElements[i].SecondElement, UserPairsElements[i].FirstElement);
                }
                if (!UserPairsElements[i].IsMoving && UserPairsElements[i].IsReturn)
                {
                    Reset(UserPairsElements[i]);
                }
            }
        }

        public void CheckMatch(List<PairElements> pairElements, int indexPairElements)
        {
            int i = indexPairElements;
            if (!pairElements[i].IsMoving)
            {
                bool isHaveFirstMatch = Grid.CheckAnyMatch(Cells[pairElements[i].FirstIndex].Cell, out int firstCountSameAuraHorizontal, out int firstCountSameAuraVertical);
                bool isHaveSecondMatch = Grid.CheckAnyMatch(Cells[pairElements[i].SecondIndex].Cell, out int secondCountSameAuraHorizontal, out int secondCountSameAuraVertical);
                if (isHaveFirstMatch || isHaveSecondMatch)
                {
                    var firstCountSameAura = new Point(firstCountSameAuraHorizontal, firstCountSameAuraVertical);
                    var secondCountSameAura = new Point(secondCountSameAuraHorizontal, secondCountSameAuraVertical);
                    CheckedCombination.Invoke(this, new CombinationEventArgs(pairElements[i], firstCountSameAura, secondCountSameAura));
                }
            }
        }

        public void CheckMatch(int indexPairElements)
        {
            int i = indexPairElements;
            if (!UserPairsElements[i].IsMoving && !UserPairsElements[i].IsReturn)
            {
                bool isHaveFirstMatch = Grid.CheckAnyMatch(Cells[UserPairsElements[i].FirstIndex].Cell, out int firstCountSameAuraHorizontal, out int firstCountSameAuraVertical);
                bool isHaveSecondMatch = Grid.CheckAnyMatch(Cells[UserPairsElements[i].SecondIndex].Cell, out int secondCountSameAuraHorizontal, out int secondCountSameAuraVertical);
                Cells[UserPairsElements[i].FirstIndex].Element.ClearHighlight();
                Cells[UserPairsElements[i].SecondIndex].Element.ClearHighlight();
                if (!isHaveFirstMatch && !isHaveSecondMatch)
                {
                    UserPairsElements[i].IsMoving = true;
                    UserPairsElements[i].IsReturn = true;
                }
                else
                {
                    var firstCountSameAura = new Point(firstCountSameAuraHorizontal, firstCountSameAuraVertical);
                    var secondCountSameAura = new Point(secondCountSameAuraHorizontal, secondCountSameAuraVertical);
                    CheckedCombination.Invoke(this, new CombinationEventArgs(UserPairsElements[i], firstCountSameAura, secondCountSameAura));
                    Reset(UserPairsElements[i]);
                }
            }
        }

        public void Reset(PairElements pairElements)
        {
            pairElements.FirstElement.FirstClick += FirstClick;
            pairElements.SecondElement.SecondClick += SecondClick;
            UserPairsElements.Remove(pairElements);
        }

        public void MoveElements(PairElements pairElements, int firstIndex, int secondIndex)
        {
            if (pairElements.IsMoving)
            {
                bool isFirstMoved = pairElements.FirstElement.Move(Cells[firstIndex].Position, Cells[secondIndex].Position);
                bool isSecondMoved = pairElements.SecondElement.Move(Cells[secondIndex].Position, Cells[firstIndex].Position);
                if (isFirstMoved && isSecondMoved)
                {
                    pairElements.IsMoving = false;
                }
            }
        }

        public void MoveElement(PairElements pairElements, int firstIndex, int secondIndex)
        {
            if (pairElements.IsMoving)
            {
                bool? isMoved = pairElements.SecondElement?.Move(Cells[firstIndex].Position, Cells[secondIndex].Position);
                if (isMoved == true)
                {
                    pairElements.IsMoving = false;
                }
            }
        }

        public void SwapElements(PairElements pairElements, ElementGameObject firstElement, ElementGameObject secondElement)
        {
            if (!pairElements.IsMoving)
            {
                Cells[pairElements.FirstIndex].ChangeElementGameObject(secondElement);
                Cells[pairElements.SecondIndex].ChangeElementGameObject(firstElement);
            }
        }

        public void CheckHighlight()
        {
            var cells = Cells[firstIndex].GetNearbodyCells();
            isMoving = cells.Where(cell => cell != null).Any(cell => cell.CurrentElement == secondElement.Element);
            if (isMoving)
            {
                UserPairsElements.Add(new PairElements(firstElement, secondElement, firstIndex, secondIndex, false, isMoving));
                firstElement.FirstClick -= FirstClick;
                secondElement.SecondClick -= SecondClick;
            }
            else
            {
                Cells[firstIndex].Element.ClearHighlight();
                Cells[secondIndex].Element.ClearHighlight();
            }
            firstElement = null;
            secondElement = null;
        }

        public void FirstClick(object sender, EventArgs eventArgs)
        {
            if (firstElement == null)
            {
                firstElement = (ElementGameObject)sender;
            }
            else
            {
                secondElement = (ElementGameObject)sender;
                firstIndex = Cells.IndexOf(Cells.FirstOrDefault(cell => cell?.Element == firstElement));
                secondIndex = Cells.IndexOf(Cells.FirstOrDefault(cell => cell?.Element == secondElement));
                if (firstIndex >= 0 && secondIndex >= 0)
                {
                    if (UserPairsElements.Count == 0 && !Grid.Cells[firstIndex].CurrentElement.IsLocked && !Grid.Cells[secondIndex].CurrentElement.IsLocked)
                    {
                        CheckHighlight();
                    }
                    else
                    {
                        ClearHighlight();
                    }
                }
                else
                {
                    ClearHighlight();
                }
            }
        }

        private void ClearHighlight()
        {
            firstElement.ClearHighlight();
            secondElement.ClearHighlight();
            firstElement = null;
            secondElement = null;
        }

        public void SecondClick(object sender, EventArgs eventArgs)
        {
            if (Equals(firstElement, (ElementGameObject)sender))
            {
                firstElement = null;
            }
            else
            {
                secondElement = null;
            }
        }
    }
}
