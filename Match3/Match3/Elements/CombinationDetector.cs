﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class CombinationDetector
    {
        public IElementBuilder ElementBuilder { get; private set; } = new BonusElementBuilder(new Element(Color.White, 4), new Element(Color.White, 5));

        public Image Line { get; private set; }
        public Image Bomb { get; private set; }

        public RespawnerElements RespawnerElements { get; private set; }

        public CombinationDetector(Image line, Image bomb, RespawnerElements respawnerElements)
        {
            Line = line;
            Bomb = bomb;
            RespawnerElements = respawnerElements;
        }

        public bool ExistCombinations(int countSameAuraHorizontal, int countSameAuraVertical)
        {
            return (countSameAuraHorizontal > 3 || countSameAuraVertical > 3 || countSameAuraHorizontal == 3 && countSameAuraVertical == 3);
        }

        public void GetCombinations(ElementGameObject elementGameObject, int index, int countSameAuraHorizontal, int countSameAuraVertical)
        {
            if (elementGameObject != null)
            {
                var element = elementGameObject.Element;
                if (countSameAuraHorizontal >= 3 && countSameAuraVertical >= 3 || countSameAuraHorizontal >= 5 || countSameAuraVertical >= 5)
                {
                    CreateBonus(elementGameObject, index, element, Bomb, Bonus.Bomb);
                }
                else if (countSameAuraHorizontal == 4 || countSameAuraVertical == 4)
                {
                    CreateBonus(elementGameObject, index, element, Line, Bonus.Line);
                }
            }
        }

        public void CreateBonus(ElementGameObject elementGameObject, int index, IElement element, Image imageBonus, Bonus bonus)
        {
            ((BonusElementBuilder)ElementBuilder).Generate(ref element, bonus, elementGameObject.Element.Aura);
            var image = new Image(imageBonus.TextureName) { Texture = imageBonus.Texture };
            var button = new Button(image, new Text("Font", string.Empty), elementGameObject.Position, elementGameObject.Element.Aura);
            RespawnerElements.CreateBonus(button, element, index);
        }
    }
}
