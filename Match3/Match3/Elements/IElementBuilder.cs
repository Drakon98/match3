﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace Match3
{
    public interface IElementBuilder
    {
        IElement Create();
        void Generate(ref IElement element);
    }
}