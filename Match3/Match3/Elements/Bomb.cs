﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class Bomb : Element
    {
        public double Time { get; private set; }

        public Bomb(Color aura, uint score, double time = 0.25) : base(aura, score)
        {
            Time = time;
        }
    }
}
