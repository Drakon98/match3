﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public enum Axis { Horizontal, Vertical }

    public class Line : Element
    {
        public Axis Axis { get; private set; }

        public Line(Color aura, uint score, Axis axis) : base(aura, score)
        {
            Axis = axis;
        }

    }
}
