﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class PairElements
    {
        public ElementGameObject FirstElement { get; private set; }
        public ElementGameObject SecondElement { get; private set; }
        public int FirstIndex { get; private set; }
        public int SecondIndex { get; private set; }

        public bool IsReturn { get; set; }
        public bool IsMoving { get; set; }

        public PairElements(ElementGameObject firstElement, ElementGameObject secondElement, int firstIndex, int secondIndex, bool isReturn = false, bool isMoving = false)
        {
            FirstElement = firstElement;
            SecondElement = secondElement;
            FirstIndex = firstIndex;
            SecondIndex = secondIndex;
            IsReturn = isReturn;
            IsMoving = isMoving;
        }
    }
}
