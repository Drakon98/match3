﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class Destroyer : GameObject, IDestroyer
    {
        private Vector2 position;

        public Vector2 Position { get => position; set { position = value; Image.Position = value; } }

        public Image Image { get; private set; }

        public float Speed { get; private set; }

        public Direction DirectionMove { get; private set; }

        public CellGameObject CurrentCell { get; private set; }

        public CellGameObject CurrentTarget { get; private set; }

        public bool IsStop { get; private set; }

        public Destroyer(Image image, Direction direction, CellGameObject currentCell, float speed = 0.03f)
        {
            Image = image;
            Position = currentCell.Position;
            Speed = speed;
            DirectionMove = direction;
            CurrentCell = currentCell;
        }

        public override void Initialize()
        {
            Image.Position = Position;
            CurrentTarget = CurrentCell.NeighborCells[DirectionMove];
        }

        public override void LoadContent(ContentManager content) => Image.LoadContent(content);

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch) => Image.Draw(gameTime, spriteBatch);

        public override void Update(GameTime gameTime)
        {
            DestroyElements();
            if (CurrentTarget != null)
            {
                Move();
            }
            if (CurrentTarget == null && CurrentCell.Element == null)
            {
                IsStop = true;
            }
        }

        public void DestroyElements()
        {
            Destroy(CurrentCell);
            Destroy(CurrentTarget);
        }

        private void Destroy(CellGameObject cell)
        {
            if (cell?.Element != null)
            {
                if (Image.Rectangle.Intersects(cell.Element.Button.Image.Rectangle))
                {
                    cell.DestroyElement();
                }
            }
        }

        private void Move()
        {
            var velocity = new Vector2(CurrentTarget.Position.X - CurrentCell.Position.X, CurrentTarget.Position.Y - CurrentCell.Position.Y) * Speed;
            Position += velocity;
            var startX = CurrentCell.Position.X;
            var startY = CurrentCell.Position.Y;
            var targetX = CurrentTarget.Position.X;
            var targetY = CurrentTarget.Position.Y;
            bool isEnterX = startX >= Position.X && Position.X >= targetX || startX <= Position.X && Position.X <= targetX;
            bool isEnterY = startY >= Position.Y && Position.Y >= targetY || startY <= Position.Y && Position.Y <= targetY;
            if (!isEnterX || !isEnterY)
            {
                Position = CurrentTarget.Position;
            }
            if (Position == CurrentTarget.Position)
            {
                CurrentCell = CurrentTarget;
                CurrentTarget = CurrentCell.NeighborCells[DirectionMove];
            }
        }

        public override void UnloadContent(ContentManager content) => Image.UnloadContent(content);

        public override void SetActive(bool isActive) => Image.SetActive(isActive);
    }
}
