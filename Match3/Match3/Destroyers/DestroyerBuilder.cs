﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class DestroyerBuilder
    {
        public Dictionary<Direction, Image> Destroyers { get; private set; }

        public Image Explosian { get; private set; }

        public DestroyerBuilder(Dictionary<Direction, Image> destroyers, Image explosian)
        {
            Destroyers = destroyers;
            Explosian = explosian;
        }

        public List<IDestroyer> CreateDestroyer(IElement element, CellGameObject currentCell)
        {
            List<IDestroyer> destoyers = new List<IDestroyer>();
            CreateLine(element, currentCell, destoyers);
            CreateBomb(element, currentCell, destoyers);
            foreach (var destroyer in destoyers)
            {
                ((GameObject)destroyer).Initialize();
            }
            return destoyers;
        }

        private void CreateLine(IElement element, CellGameObject currentCell, List<IDestroyer> destoyers)
        {
            if (element is Line line)
            {
                var directions = new List<Direction>();
                switch (line.Axis)
                {
                    case Axis.Horizontal: { directions.AddRange(new List<Direction>() { Direction.Left, Direction.Right }); } break;
                    case Axis.Vertical: { directions.AddRange(new List<Direction>() { Direction.Up, Direction.Down }); } break;
                }
                AddDestroyer(currentCell, destoyers, directions, 0);
                AddDestroyer(currentCell, destoyers, directions, 1);
            }
        }

        private void AddDestroyer(CellGameObject currentCell, List<IDestroyer> destoyers, List<Direction> directions, int index)
        {
            Image destroyer = Destroyers[directions[index]];
            var newDestroyer = new Image(destroyer.TextureName, Vector2.Zero, destroyer.Color) { Texture = destroyer.Texture };
            destoyers.Add(new Destroyer(newDestroyer, directions[index], currentCell));
        }

        private void CreateBomb(IElement element, CellGameObject currentCell, List<IDestroyer> destoyers)
        {
            if (element is Bomb bomb)
            {
                var image = new Image(Explosian.TextureName, Vector2.Zero, Explosian.Color) { Texture = Explosian.Texture };
                destoyers.Add(new Explosian(image, currentCell, bomb.Time));
            }
        }
    }
}
