﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class Explosian : GameObject, IDestroyer
    {
        private Vector2 position;
        public Vector2 Position { get => position; set { position = value; Image.Position = new Vector2(position.X - 200f / 2, position.Y - 200f / 2); } }

        public Image Image { get; private set; }

        public double Time { get; private set; }

        public Timer Timer { get; private set; }

        public CellGameObject CurrentCell { get; private set; }

        public bool IsStop { get; private set; }

        public Explosian(Image image, CellGameObject currentCell, double time)
        {
            Image = image;
            CurrentCell = currentCell;
            Position = currentCell.Position;
            Timer = new Timer(time);
            Time = time;
        }

        public override void Initialize()
        {
            Timer.Timeout += TimerTimeout;
            Image.Initialize();
        }

        public void TimerTimeout(object sender, EventArgs e) => DestroyElements();

        public override void LoadContent(ContentManager content) => Image.LoadContent(content);

        public override void Update(GameTime gameTime) => Timer.Run();

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch) => Image.Draw(gameTime, spriteBatch);

        public override void UnloadContent(ContentManager content) => Image.UnloadContent(content);

        public override void SetActive(bool isActive) => Image.SetActive(isActive);

        public void DestroyElements()
        {
            var cells = new List<CellGameObject>();
            AddDestroyElements(cells, CurrentCell);
            AddDestroyElements(cells, CurrentCell.NeighborCells[Direction.Up]);
            AddDestroyElements(cells, CurrentCell.NeighborCells[Direction.Down]);
            foreach (var cell in cells)
            {
                cell?.DestroyElement();
            }
            IsStop = true;
        }

        private void AddDestroyElements(List<CellGameObject> cells, CellGameObject cell)
        {
            cells.Add(cell);
            cells.Add(cell?.NeighborCells[Direction.Left]);
            cells.Add(cell?.NeighborCells[Direction.Right]);
        }
    }
}
