﻿using System;
using System.Collections.Generic;

namespace Match3
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            List<Scene> scenes = new List<Scene>()
            {
                new Scene("MainMenu", new MainMenu()),
                new Scene("Level1", new Level1())
            };
            using (var game = new Application(scenes))
            {
                game.Run();
            }
        }
    }
#endif
}
