﻿using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class AudioFile
    {
        public string SongName { get; private set; }

        public Song Song { get; private set; }

        public AudioFile(string songName, Song song)
        {
            SongName = songName;
            Song = song;
        }
    }
}
