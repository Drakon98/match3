﻿using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3
{
    public class Playlist
    {
        private float volume;
        private bool isRepeated;
        public List<AudioFile> AudioFiles { get; private set; } = new List<AudioFile>();

        public float Volume { get => volume; set { volume = value; MediaPlayer.Volume = volume; } }

        public bool IsRepeated { get => isRepeated; set { isRepeated = value; MediaPlayer.IsRepeating = isRepeated; } }

        public Playlist(List<AudioFile> audioFiles, float volume = 1f, bool isRepeated = true)
        {
            AudioFiles = audioFiles;
            Volume = volume;
            IsRepeated = isRepeated;
        }

        public void AddAudioFile(AudioFile audioFile) => AudioFiles.Add(audioFile);

        public void RemoveAudioFile(AudioFile audioFile) => AudioFiles.Remove(audioFile);

        public void Play() => MediaPlayer.Play(AudioFiles[0].Song);

        public void Stop() => MediaPlayer.Stop();

        public void Resume() => MediaPlayer.Resume();

    }
}
