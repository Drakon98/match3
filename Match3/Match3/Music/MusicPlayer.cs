﻿using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Match3
{
    public class MusicPlayer : IComponent, IActive
    {
        private List<string> songsName = new List<string>();

        public event EventHandler Activated;
        public event EventHandler Disactivated;

        public Playlist Playlist { get; private set; }

        public bool IsActive { get; private set; }

        public MusicPlayer(List<string> songsName) => this.songsName = songsName;

        private void LoadRangeSongs(List<string> songsName, ContentManager content)
        {
            var audioFiles = new List<AudioFile>();
            foreach (var songName in songsName)
            {
                audioFiles.Add(new AudioFile(songName, content.Load<Song>(songName)));
            }
            Playlist = new Playlist(audioFiles);
            Playlist.Play();
        }

        public void Initialize() { }

        public void LoadContent(ContentManager content) => LoadRangeSongs(songsName, content);

        public void Update(GameTime gameTime) { }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch) { }

        public void UnloadContent(ContentManager content) => content.Unload();

        public void SetActive(bool isActive)
        {
            IsActive = isActive;
            if(isActive)
            {
                MediaPlayer.Resume();
                Activated?.Invoke(this, new EventArgs());
            }
            else
            {
                MediaPlayer.Stop();
                Disactivated?.Invoke(this, new EventArgs());
            }
        }
    }
}
